using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace RazorPagesDemo.Pages
{
    public class timeModel : PageModel
    {
        public JsonResult OnGet()
        {
            int epoch = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

            string pod = "";
            DateTime time = new DateTime();
            time = DateTime.Now;

            if (time.Hour >= 6 && time.Hour < 12)
            {
                pod = "Morning";
            }
            else if (time.Hour >= 12 && time.Hour < 18)
            {
                pod = "Afternoon";
            }
            else if (time.Hour >= 18 && time.Hour < 21)
            {
                pod = "Evening";
            }
            else
            {
                pod = "Night";
            }

            var j = new
            {
                epoch = epoch,
                formatted = new
                {
                    simpledatetime = DateTime.Now.ToString("dd MMMM hh:mm tt"),
                    datetime = DateTime.Now.ToString("dd MMMM yyyy hh:mm tt"),
                    date = DateTime.Now.ToString("dd MMMM yyyy"),
                    time = DateTime.Now.ToString("h:mm tt"),
                    simpletime = DateTime.Now.ToString("h:mm"),
                    simpletimefull = DateTime.Now.ToString("h:mm:ss")
                },
                parts = new
                {
                    year = DateTime.Now.Year,
                    month = DateTime.Now.Month,
                    day = DateTime.Now.Day,
                    hour = int.Parse(DateTime.Now.ToString("hh")),
                    minute = DateTime.Now.Minute,
                    second = DateTime.Now.Second,
                    milisecond = DateTime.Now.Millisecond,
                    monthname = DateTime.Now.ToString("MMMM"),
                    dayname = DateTime.Now.ToString("dddd"),
                    ampm = DateTime.Now.ToString("tt"),
                    militaryHour = DateTime.Now.Hour,
                    partofday = pod
                }
            };

            JsonResult nj = new JsonResult(j);

            return nj;
        }
    }
}
